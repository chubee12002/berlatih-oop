<?php
require('animal.php');
require('Frog.php');
require('Ape.php');


echo "Release 0 <br>\n<br>\n";
$sheep = new Animal("shaun");

echo "Nama Hewan :" . $sheep->name . "<br>\n"; // "shaun"
echo "Jumlah Kaki :" . $sheep->legs . "<br>\n"; // 2
echo "Berdarah Dingin :" . $sheep->cold_blooded . "<br>\n<br>\n"; // false

echo "Release 1 <br>\n<br>\n";
$sungokong = new Ape("kera sakti");
echo "Nama Hewan :" . $sungokong->name . "<br>\n"; // "shaun"
echo "Jumlah Kaki :" . $sungokong->legs . "<br>\n"; // 2
echo "Berdarah Dingin :" . $sungokong->cold_blooded . "<br>\n"; // false 
$sungokong->yell(); // "Auooo"

echo "<br>\n<br>\n";

$kodok = new Frog("buduk");
echo "Nama Hewan :" . $kodok->name . "<br>\n"; // "shaun"
echo "Jumlah Kaki :" . $kodok->legs . "<br>\n"; // 2
echo "Berdarah Dingin :" . $kodok->cold_blooded . "<br>\n"; // false
$kodok->jump(); // "hop hop"
